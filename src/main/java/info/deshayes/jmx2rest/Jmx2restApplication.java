package info.deshayes.jmx2rest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Jmx2restApplication {

	public static void main(String[] args) {
		SpringApplication.run(Jmx2restApplication.class, args);
	}
}
