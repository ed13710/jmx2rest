package info.deshayes.jmx2rest.connection;

import java.util.concurrent.atomic.AtomicLong;

public class MBeanConnection {

	private static AtomicLong idGenerator = new AtomicLong();
	
	public static Long getNextId() {
		return idGenerator.getAndIncrement();
	}
	
	private Long id;
	private boolean connected;
	private boolean autoconnect;
	private boolean deleted = false;
	private String jmxUrl;
	
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public Long getId() {
		return id;
	}
	
	public boolean isConnected() {
		return connected;
	}
	public void setConnected(boolean connected) {
		this.connected = connected;
	}
	
	public void setJmxUrl(String jmxUrl) {
		this.jmxUrl = jmxUrl;
	}
	public String getJmxUrl() {
		return jmxUrl;
	}
	
	public boolean isAutoconnect() {
		return autoconnect;
	}
	
	public void setAutoconnect(boolean autoconnect) {
		this.autoconnect = autoconnect;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	public boolean isDeleted() {
		return deleted;
	}
}
