package info.deshayes.jmx2rest.connection;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import javax.management.AttributeChangeNotification;
import javax.management.MBeanServerConnection;
import javax.management.Notification;
import javax.management.NotificationListener;
import javax.management.remote.JMXConnectionNotification;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;
import javax.management.remote.JMXServiceURL;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

@RestController
public class JMXConnectionController {

	private static final Logger log = LoggerFactory.getLogger(JMXConnectionController.class);

	private Map<Long, MBeanConnectionWrapper> connectionsById = new ConcurrentHashMap<>();
	private ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();

	@GetMapping("/connections")
	List<MBeanConnection> all() {
		List<MBeanConnection> result = new ArrayList<>();
		connectionsById.values().stream().forEach( s -> {result.add(s.getMBeanConnection());});
		return result;
	}
	
	public static class ClientListener implements NotificationListener {

	    public void handleNotification(Notification notification,
	            Object handback) {
	        echo("\nReceived notification:");
	        echo("\tClassName: " + notification.getClass().getName());
	        echo("\tSource: " + notification.getSource());
	        echo("\tType: " + notification.getType());
	        echo("\tMessage: " + notification.getMessage());
	        if (notification instanceof AttributeChangeNotification) {
	            AttributeChangeNotification acn =
	                (AttributeChangeNotification) notification;
	            echo("\tAttributeName: " + acn.getAttributeName());
	            echo("\tAttributeType: " + acn.getAttributeType());
	            echo("\tNewValue: " + acn.getNewValue());
	            echo("\tOldValue: " + acn.getOldValue());
	        }
	    }
	}    

	@PutMapping("/connection")
	MBeanConnection connect(@RequestBody MBeanConnection connection) {
		MBeanConnection mBeanConnection = new MBeanConnection();
        mBeanConnection.setId(MBeanConnection.getNextId());
        mBeanConnection.setJmxUrl(connection.getJmxUrl());
        MBeanConnectionWrapper mBeanConnectionWrapper = new MBeanConnectionWrapper(mBeanConnection);
        connectionsById.put(mBeanConnection.getId(), mBeanConnectionWrapper);

        executorService.schedule(new Connect(mBeanConnectionWrapper), 0, TimeUnit.SECONDS);

        return mBeanConnection;

	}

	private class Connect implements Runnable {
        private final MBeanConnectionWrapper mBeanConnectionWrapper;

        Connect(MBeanConnectionWrapper mBeanConnectionWrapper) {
            this.mBeanConnectionWrapper = mBeanConnectionWrapper;
        }

        @Override
        public void run() {
            try {
                if (mBeanConnectionWrapper.getMBeanConnection().isDeleted() ) {
                    return;
                }
                if (mBeanConnectionWrapper.getJmxConnector() == null) {
                    JMXServiceURL url = new JMXServiceURL(mBeanConnectionWrapper.getMBeanConnection().getJmxUrl());
                    final JMXConnector jmxc = JMXConnectorFactory.connect(url, null);

                    mBeanConnectionWrapper.setJmxConnector(jmxc);
                    jmxc.addConnectionNotificationListener(new NotificationListener() {

                        @Override
                        public void handleNotification(Notification notification, Object handback) {
                            log.info("notification for {} : {} ", handback, notification);
                            if (JMXConnectionNotification.CLOSED.equals(notification.getType())
                                    || JMXConnectionNotification.FAILED.equals(notification.getType()) ) {
                                mBeanConnectionWrapper.getMBeanConnection().setConnected(false);
                                mBeanConnectionWrapper.setJmxConnector(null);
                                log.info("JMX Connection is closed");
                                if (((MBeanConnectionWrapper)handback).getMBeanConnection().isAutoconnect()) {
                                    log.info("rescheduling connection for {}", mBeanConnectionWrapper.getJmxConnector());
                                    executorService.schedule(new Connect(mBeanConnectionWrapper), 5, TimeUnit.SECONDS);
                                }
                            }

                        }
                    }, null, mBeanConnectionWrapper);
                }


                mBeanConnectionWrapper.getJmxConnector().connect();

                MBeanServerConnection mbsc =
                        mBeanConnectionWrapper.getJmxConnector().getMBeanServerConnection();

                mBeanConnectionWrapper.getMBeanConnection().setConnected(true);
                echo("\nDomains:");
                String domains[] = mbsc.getDomains();
                Arrays.sort(domains);
                for (String domain : domains) {
                    echo("\tDomain = " + domain);
                }

//						mbsc.addNotificationListener(name, new ClientListener(), null, mBeanConnectionWrapper);
                mBeanConnectionWrapper.setMBeanServerConnection(mbsc);
            } catch (IOException e) {
                log.info("connection failed for {}", mBeanConnectionWrapper.getJmxConnector(), e);
                if (mBeanConnectionWrapper.getMBeanConnection().isAutoconnect()) {
                    log.info("rescheduling connection for {}", mBeanConnectionWrapper.getJmxConnector());
                    executorService.schedule(new Connect(mBeanConnectionWrapper), 5, TimeUnit.SECONDS);
                }
            }
        }
    }

	public static void echo(String string) {
		log.info(string);
		
	}

	@PutMapping("/connection/{id}/disconnect")
	MBeanConnection disconnect(@PathVariable Long id) {
		MBeanConnectionWrapper mBeanConnection = connectionsById.get(id);
		if (mBeanConnection != null) {
            disconnect(mBeanConnection);
        }
		return mBeanConnection.getMBeanConnection();
	}

    @PutMapping("/connection/{id}/connect")
    MBeanConnection connect(@PathVariable Long id) {
        MBeanConnectionWrapper mBeanConnectionWrapper = connectionsById.get(id);
        if (mBeanConnectionWrapper != null) {
            if (!mBeanConnectionWrapper.getMBeanConnection().isConnected()) {
                executorService.schedule(new Connect(mBeanConnectionWrapper), 0, TimeUnit.SECONDS);
            }
            return mBeanConnectionWrapper.getMBeanConnection();
        }
        return null;

    }

    private void disconnect(MBeanConnectionWrapper mBeanConnection) {
        try {
            mBeanConnection.getJmxConnector().close();

        } catch (IOException e) {
            log.info("disconnection failed for {}", mBeanConnection.getJmxConnector(), e);
        }
    }

    @GetMapping("/connection/{id}")
	MBeanConnection get(@PathVariable Long id) {
		MBeanConnectionWrapper mBeanConnection = connectionsById.get(id);
		return mBeanConnection.getMBeanConnection();
	}

    @DeleteMapping("/connection/{id}")
    void delete(@PathVariable Long id) {
        MBeanConnectionWrapper mBeanConnection = connectionsById.remove(id);
        if(mBeanConnection != null) {
            mBeanConnection.getMBeanConnection().setDeleted(true);
            disconnect(mBeanConnection);
        }
       return;
    }

}
