package info.deshayes.jmx2rest.connection;

import javax.management.MBeanServerConnection;
import javax.management.remote.JMXConnector;

public class MBeanConnectionWrapper {

	private MBeanConnection mBeanConnection;
	
	private JMXConnector jmxConnector;
	
	private MBeanServerConnection mbsc;


	public MBeanConnectionWrapper(MBeanConnection beanConnection) {
		super();
		this.mBeanConnection = beanConnection;

	}

	public MBeanConnectionWrapper(MBeanConnection beanConnection, JMXConnector jmxConnector) {
		super();
		this.mBeanConnection = beanConnection;
		this.jmxConnector = jmxConnector;
	}
	
	public JMXConnector getJmxConnector() {
		return jmxConnector;
	}

    public void setJmxConnector(JMXConnector jmxConnector) {
        this.jmxConnector = jmxConnector;
    }

    public MBeanConnection getMBeanConnection() {
		return mBeanConnection;
	}
	public void setMBeanServerConnection(MBeanServerConnection mbsc) {
		this.mbsc = mbsc;
	}
	
	public MBeanServerConnection getMBeanServerConnection() {
		return mbsc;
	}
	
}
